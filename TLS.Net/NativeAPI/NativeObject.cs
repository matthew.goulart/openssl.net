﻿using System;

namespace TLS.Net.NativeAPI
{
    public abstract class NativeObject : IDisposable
    {
        public NativeObject(IntPtr handle)
        {
            Handle = handle;
        }

        public IntPtr Handle { get; }

        public abstract void Dispose();

        public static implicit operator IntPtr(NativeObject obj)
        {
            return obj.Handle;
        }

        ~NativeObject()
        {
        }
    }
}