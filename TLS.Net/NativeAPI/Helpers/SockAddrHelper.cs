using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace TLS.Net.NativeAPI.Helpers
{
    public static class SockAddrHelper
    {
        public static IPEndPoint SockAddrToIPEndPoint(IntPtr sockAddrPtr)
        {
            var sockAddr = Marshal.PtrToStructure<SockAddr>(sockAddrPtr);
            switch ((SockAddrFamily) sockAddr.Family)
            {
                case SockAddrFamily.Inet:
                {
                    var sockAddrIn = Marshal.PtrToStructure<SockAddrIn>(sockAddrPtr);

                    if (BitConverter.IsLittleEndian)
                    {
                        sockAddrIn.Port = SwapBytes(sockAddrIn.Port);
                    }
                    
                    return new IPEndPoint(sockAddrIn.Addr, sockAddrIn.Port);
                }

                case SockAddrFamily.Inet6:
                {
                    var sockAddrIn6 = Marshal.PtrToStructure<SockAddrIn6>(sockAddrPtr);
                    return new IPEndPoint(new IPAddress(sockAddrIn6.Addr), sockAddrIn6.Port);
                }

                default:
                    throw new Exception($"Non-IP address family: {sockAddr.Family}");
            }
        }

        public static IntPtr IPEndPointToSockAddr(IPEndPoint endPoint)
        {
            var sockAddrPtr = Marshal.AllocHGlobal(Marshal.SizeOf<SockAddr>());

            switch (endPoint.AddressFamily)
            {
                case AddressFamily.InterNetwork:
                {
                    var sockAddrIn = new SockAddrIn();
                    sockAddrIn.Family = (ushort) AddressFamily.InterNetwork;
                    sockAddrIn.Addr = (uint) endPoint.Address.Address; //TODO: find alternative to deprecated address
                    sockAddrIn.Port = (ushort) endPoint.Port;

                    Marshal.StructureToPtr(sockAddrIn, sockAddrPtr, false);

                    break;
                }

                case AddressFamily.InterNetworkV6:
                {
                    var sockAddrIn6 = new SockAddrIn6();
                    sockAddrIn6.Family = (ushort) AddressFamily.InterNetworkV6;
                    sockAddrIn6.Addr = endPoint.Address.GetAddressBytes();
                    sockAddrIn6.Port = (ushort) endPoint.Port;

                    Marshal.StructureToPtr(sockAddrIn6, sockAddrPtr, false);

                    break;
                }

                default:
                    throw new Exception($"Non-IP address family: {endPoint.AddressFamily}");
            }

            return sockAddrPtr;
        }
        
        private static ushort SwapBytes(ushort x)
        {
            return (ushort)((ushort)((x & 0xff) << 8) | ((x >> 8) & 0xff));
        }
    }

    public enum SockAddrFamily
    {
        Inet = 2,
        Inet6 = 23
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SockAddr
    {
        public ushort Family;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
        public byte[] Data;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SockAddrIn
    {
        public ushort Family;
        public ushort Port;
        public uint Addr;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] Zero;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SockAddrIn6
    {
        public ushort Family;
        public ushort Port;
        public uint FlowInfo;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] Addr;

        public uint ScopeId;
    }
}