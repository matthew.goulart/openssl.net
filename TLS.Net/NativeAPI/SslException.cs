﻿using System;
using TLS.Net.NativeAPI;

namespace TLS.Net.NativeAPI
{
    public class SslException : Exception
    {
        public SslException()
        {
        }

        public SslException(ulong errCode)
        {
            ErrorCode = errCode;
        }

        public SslException(string message)
            : base(message)
        {
        }

        public SslException(string message, Exception innerEx)
            : base(message, innerEx)
        {
        }

        public ulong ErrorCode { get; }
        public override string Message => ErrorHandler.GetErrorString(ErrorCode);
    }
}