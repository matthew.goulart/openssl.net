namespace TLS.Net.NativeAPI.SSL
{
    public enum EncryptionMethod
    {
        AESGCM_128,
        AESGCM_256,
        AESCCM_128,
        AESCCM_128_8,
        CHACHA20_POLY1305_256
    }
}