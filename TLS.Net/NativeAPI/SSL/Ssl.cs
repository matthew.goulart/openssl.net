﻿using System;
using System.Net.Sockets;
using TLS.Net.NativeAPI.BIO;

namespace TLS.Net.NativeAPI.SSL
{
    public class Ssl : NativeObject
    {
        public const int TLS_MAX_RECORD_SIZE = 4096;
        private Bio _rBio;
        private Bio _wBio;

        public Ssl(IntPtr handle)
            : base(handle)
        {
        }

        /// <summary>
        ///     Initializes a new <see cref="Ssl" /> object based on the settings in ctx.
        /// </summary>
        /// <param name="ctx">The <see cref="Context" /> to use for initialization of the <see cref="Ssl" /> object</param>
        public Ssl(Context ctx)
            : base(Native.SSL_new(ctx))
        {
        }

        /// <summary>
        ///     Initializes a new <see cref="Ssl" /> object based on the settings in ctx.
        ///     Sets both the <see cref="ReadBio" /> and <see cref="WriteBio" /> to bio
        /// </summary>
        /// <param name="ctx">The <see cref="Context" /> to use for initialization of the <see cref="Ssl" /> object</param>
        /// <param name="bio">The <see cref="Bio" /> to use for <see cref="ReadBio" /> and <see cref="WriteBio" /></param>
        public Ssl(Context ctx, Bio bio)
            : base(Native.SSL_new(ctx))
        {
            ReadBio = WriteBio = bio;
        }

        /// <summary>
        ///     Initializes a new <see cref="Ssl" /> based on the settings in ctx.
        ///     Sets both the <see cref="ReadBio" /> and <see cref="WriteBio" />
        /// </summary>
        /// <param name="ctx">The <see cref="Context" /> to use for initialization of the <see cref="Ssl" /> object</param>
        /// <param name="rBio">The <see cref="Bio" /> to use for <see cref="ReadBio" /></param>
        /// <param name="wBio">The <see cref="Bio" /> to use for <see cref="WriteBio" /></param>
        public Ssl(Context ctx, Bio rBio, Bio wBio)
            : base(Native.SSL_new(ctx))
        {
            ReadBio = rBio;
            WriteBio = wBio;
        }

        /// <summary>
        ///     Initializes a new <see cref="Ssl" /> based on the settings in ctx.
        ///     Sets the <see cref="Fd" /> to the <see cref="Socket" /> handle.
        /// </summary>
        /// <param name="ctx">The <see cref="Context" /> to use for initialization of the <see cref="Ssl" /> object</param>
        /// <param name="socket">The <see cref="Socket" /> to set as the file descriptor</param>
        public Ssl(Context ctx, Socket socket)
            : base(Native.SSL_new(ctx))
        {
            Fd = socket.Handle;
        }
        
        #region Properties

        public SslHandshakeState HandshakeState => Native.SSL_get_state(Handle);
        public bool IsInitFinished => Native.SSL_is_init_finished(Handle) == 1;

        /// <summary>
        ///     SSL_get/set_fd
        /// </summary>
        /// <exception cref="SslException"></exception>
        public IntPtr Fd
        {
            get
            {
                var ptr = Native.SSL_get_fd(Handle);

                if (ptr < 0)
                    throw new SslException(ErrorHandler.GetError());

                return new IntPtr(ptr);
            }

            set
            {
                if (Native.SSL_set_fd(Handle, value) != 1)
                    throw new SslException(ErrorHandler.GetError());
            }
        }

        /// <summary>
        ///     SSL_get/set_max_protocol_version (macro)
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_get_max_proto_version" />
        /// </summary>
        public ProtocolVersion MaxProtocolVersion
        {
            get => (ProtocolVersion) Native.SSL_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_GET_MAX_PROTO_VERSION,
                0, IntPtr.Zero);
            set => Native.SSL_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_SET_MAX_PROTO_VERSION, (int) value,
                IntPtr.Zero);
        }

        /// <summary>
        ///     SSL_get/set_min_protocol_version
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_get_min_proto_version" />
        /// </summary>
        public ProtocolVersion MinProtocolVersion
        {
            get => (ProtocolVersion) Native.SSL_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_GET_MIN_PROTO_VERSION,
                0, IntPtr.Zero);
            set => Native.SSL_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_SET_MIN_PROTO_VERSION, (int) value,
                IntPtr.Zero);
        }

        /// <summary>
        ///     SSL_is_server <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_is_server" />
        /// </summary>
        public bool IsServer
        {
            get
            {
                if (Native.SSL_is_server(Handle) == 1)
                    return true;

                return false;
            }
        }

        /// <summary>
        ///     SSL_get/set_rbio <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_get_rbio" />
        /// </summary>
        /// <exception cref="SslException"></exception>
        public Bio ReadBio
        {
            get
            {
                var ptr = Native.SSL_get_rbio(Handle);

                return Bio.IntPtrToBio(ptr);
            }

            set
            {
                _rBio = value;
                Native.SSL_set0_rbio(Handle, value);
            }
        }

        /// <summary>
        ///     SSL_get/set_wbio <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_get_wbio" />
        /// </summary>
        /// <exception cref="SslException"></exception>
        public Bio WriteBio
        {
            get
            {
                var ptr = Native.SSL_get_wbio(Handle);

                return Bio.IntPtrToBio(ptr);
            }

            set
            {
                _wBio = value;
                Native.SSL_set0_wbio(Handle, value);
            }
        }

        #endregion

        public long SetMode(SslModes mode)
        {
            return Native.SSL_set_mode(Handle, (long) mode);
        }

        public long SetOptions(SslOptions opts)
        {
            return Native.SSL_set_options(Handle, (long) opts);
        }

        public int SetCipherList(string list)
        {
            return Native.SSL_set_cipher_list(Handle, list);
        }

        public int SetCiphersuites(string suites)
        {
            return Native.SSL_set_ciphersuites(Handle, suites);
        }

        public void SetConnectState()
        {
            Native.SSL_set_connect_state(Handle);
        }

        public void SetAcceptState()
        {
            Native.SSL_set_accept_state(Handle);
        }

        public int Connect()
        {
            return Native.SSL_connect(Handle);
        }

        public int Accept()
        {
            return Native.SSL_accept(Handle);
        }

        public static bool DtlsListen(Ssl ssl, BioAddress bioAddr)
        {
            var ret = Native.DTLSv1_listen(ssl, bioAddr);

            if (ret < 0)
                throw new SslException(ErrorHandler.GetError());

            if (ret == 0)
                return false;

            return true;
        }

        public int DoHandshake()
        {
            return Native.SSL_do_handshake(Handle);
        }

        public int Read(byte[] buf, int length)
        {
            return Native.SSL_read(Handle, buf, length);
        }

        public int Peek(byte[] buf, int length)
        {
            return Native.SSL_peek(Handle, buf, length);
        }

        public int Write(byte[] buf, int length)
        {
            return Native.SSL_write(Handle, buf, length);
        }

        public SslError GetError(int ret)
        {
            return (SslError) Native.SSL_get_error(Handle, ret);
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public int Shutdown()
        {
            return Native.SSL_shutdown(Handle);
        }

        ~Ssl()
        {
            Native.SSL_free(Handle);
        }


    }
}