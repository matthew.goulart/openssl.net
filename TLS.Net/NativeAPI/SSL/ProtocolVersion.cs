namespace TLS.Net.NativeAPI.SSL
{
    public enum ProtocolVersion
    {
        SSL2_VERSION = 0x0002,
        SSL3_VERSION = 0x0300,
        TLS1_VERSION = 0x0301,
        TLS1_1_VERSION = 0x0302,
        TLS1_2_VERSION = 0x0303,
        TLS1_3_VERSION = 0x0304,
        DTLS1_VERSION = 0xFEFF,
        DTLS1_2_VERSION = 0xFEFD,
        HIGHEST_OR_LOWEST = 0
    }
}