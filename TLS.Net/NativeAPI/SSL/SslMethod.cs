﻿using System;

namespace TLS.Net.NativeAPI.SSL
{
    public class SslMethod : NativeObject
    {
        private SslMethod(IntPtr handle)
            : base(handle)
        {
        }

        //Version-agnostic methods
        public static SslMethod TLS => new SslMethod(Native.TLS_method());
        public static SslMethod TLS_Server => new SslMethod(Native.TLS_server_method());
        public static SslMethod TLS_Client => new SslMethod(Native.TLS_client_method());

        public static SslMethod DTLS => new SslMethod(Native.DTLS_method());
        public static SslMethod DTLS_Server => new SslMethod(Native.DTLS_server_method());
        public static SslMethod DTLS_Client => new SslMethod(Native.DTLS_client_method());

        //Deprecated methods
        public static SslMethod SSL_V2 => new SslMethod(Native.SSLv2_method());
        public static SslMethod SSL_V2_Client => new SslMethod(Native.SSLv2_client_method());
        public static SslMethod SSL_V2_Server => new SslMethod(Native.SSLv2_server_method());

        public static SslMethod SSL_V3 => new SslMethod(Native.SSLv3_method());
        public static SslMethod SSL_V3_Client => new SslMethod(Native.SSLv3_client_method());
        public static SslMethod SSL_V3_Server => new SslMethod(Native.SSLv3_server_method());

        public static SslMethod TLS_V1 => new SslMethod(Native.TLSv1_method());
        public static SslMethod TLS_V1_Client => new SslMethod(Native.TLSv1_client_method());
        public static SslMethod TLS_V1_Server => new SslMethod(Native.TLSv1_server_method());

        public static SslMethod TLS_V1_1 => new SslMethod(Native.TLSv1_1_method());
        public static SslMethod TLS_V1_1_Client => new SslMethod(Native.TLSv1_1_client_method());
        public static SslMethod TLS_V1_1_Server => new SslMethod(Native.TLSv1_1_server_method());

        public static SslMethod TLS_V1_2 => new SslMethod(Native.TLSv1_2_method());
        public static SslMethod TLS_V1_2_Client => new SslMethod(Native.TLSv1_2_client_method());
        public static SslMethod TLS_V1_2_Server => new SslMethod(Native.TLSv1_2_server_method());

        public static SslMethod DTLS_V1 => new SslMethod(Native.DTLSv1_method());
        public static SslMethod DTLS_V1_Client => new SslMethod(Native.DTLSv1_client_method());
        public static SslMethod DTLS_V1_Server => new SslMethod(Native.DTLSv1_server_method());

        public static SslMethod DTLS_V1_2 => new SslMethod(Native.DTLSv1_2_method());
        public static SslMethod DTLS_V1_2_Client => new SslMethod(Native.DTLSv1_2_client_method());
        public static SslMethod DTLS_V1_2_Server => new SslMethod(Native.DTLSv1_2_server_method());

        public override void Dispose()
        {
            //Nothing to clean up here...
        }
    }
}