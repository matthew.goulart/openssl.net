namespace TLS.Net.NativeAPI.SSL
{
    public enum KeyExchangeMethod
    {
        Any,
        RSA,
        ECDHE
    }
}