namespace TLS.Net.NativeAPI.SSL
{
    public enum MACType
    {
        SHA256,
        SHA384,
        AEAD
    }
}