﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TLS.Net.NativeAPI.SSL
{
    public class Context : NativeObject
    {
        public delegate ArraySegment<byte> GenerateCookieCallback(Ssl ssl);

        public delegate bool SslVerifyCallback(int ok, X509Store store);

        public delegate bool VerifyCookieCallback(Ssl ssl, byte[] cookie);

        private GenerateCookieCallback _genCookieCallback;

        //Prevent callbacks from being collected
        private Native.GenerateCookieDelegate _genCookieDelegate;

        private SslVerifyCallback _verifyCallback;
        private VerifyCookieCallback _verifyCookieCallback;
        private Native.VerifyCookieDelegate _verifyCookieDelegate;
        private Native.VerifyCertCallback _verifyDelegate;

        private int verifyCookieResult;

        public Context(SslMethod method)
            : base(Native.SSL_CTX_new(method.Handle))
        {
        }

        /// <summary>
        ///     SSL_CTX_set_verify <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_verify.html" />
        /// </summary>
        /// <param name="kind"></param>
        /// <param name="callback"></param>
        public void SetVerify(SslVerificationKind kind, SslVerifyCallback callback)
        {
            _verifyCallback = callback;
            _verifyDelegate = VerifyCertThunk;

            Native.SSL_CTX_set_verify(Handle, (int) kind, _verifyDelegate);
        }

        private int VerifyCertThunk(int ok, IntPtr x509_store_ctx)
        {
            if (_verifyCallback == null)
                return Native.FAIL;

            return _verifyCallback(ok, null) ? 1 : 0; // TODO Implement x509 store
        }

        /// <summary>
        ///     SSL_CTX_set_verify_depth <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_verify_depth.html" />
        /// </summary>
        /// <param name="depth">The depth</param>
        public void SetVerifyDepth(int depth)
        {
            Native.SSL_CTX_set_verify_depth(Handle, depth);
        }

        /// <summary>
        ///     SSL_CTX_load_verify_locations.
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_load_verify_locations.html" />
        /// </summary>
        /// <param name="file">Path to certs file</param>
        /// <param name="path">Path containing cert files</param>
        /// <exception cref="FileNotFoundException">The cert file could not be found</exception>
        /// <exception cref="DirectoryNotFoundException">The cert directory could not be found</exception>
        /// <exception cref="SslException">An error occured in OpenSSL</exception>
        public void LoadVerifyLocations(string file, string path)
        {
            if (file != null && !File.Exists(file))
                throw new FileNotFoundException($"Cert file: {file} does not exist");

            if (path != null && !Directory.Exists(path))
                throw new DirectoryNotFoundException($"Cert directory: {path} does not exist");

            if (Native.SSL_CTX_load_verify_locations(Handle, file, path) != 1)
                throw new SslException("An error occured while trying to load certificates.");
        }

        /// <summary>
        ///     SSL_CTX_use_certificate_file.
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_use_certificate_file.html" />
        /// </summary>
        /// <param name="file">Path to cert file</param>
        /// <param name="type"><see cref="CertificateType" /> of the cert file</param>
        /// <exception cref="SslException"></exception>
        public void UseCertificateFile(string file, CertificateType type)
        {
            var ret = Native.SSL_CTX_use_certificate_file(Handle, file, (int) type);

            if (ret < 1)
                throw new SslException(ErrorHandler.GetError());
        }

        /// <summary>
        ///     SSL_CTX_use_PrivateKey_file.
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_use_certificate_file.html" />
        /// </summary>
        /// <param name="file">Path to key file</param>
        /// <param name="type"><see cref="CertificateType" /> of the key file</param>
        /// <exception cref="SslException"></exception>
        public void UsePrivateKeyFile(string file, CertificateType type)
        {
            var ret = Native.SSL_CTX_use_PrivateKey_file(Handle, file, (int) type);

            if (ret < 1)
                throw new SslException(ErrorHandler.GetError());
        }

        /// <summary>
        ///     SSL_CTX_set_cookie_generate_cb (no docs)
        /// </summary>
        /// <param name="cb"></param>
        public void SetGenerateCookieCallback(GenerateCookieCallback cb)
        {
            _genCookieCallback = cb;
            _genCookieDelegate = GenerateCookieThunk;

            Native.SSL_CTX_set_cookie_generate_cb(Handle, _genCookieDelegate);
        }

        private int GenerateCookieThunk(IntPtr sslPtr, IntPtr cookie, ref int cookie_len)
        {
            if (_genCookieCallback == null)
                return Native.FAIL;

            var ssl = new Ssl(sslPtr);

            var ret = _genCookieCallback(ssl);

            Marshal.Copy(ret.Array, 0, cookie, ret.Count);

            cookie_len = ret.Count;

            return Native.SUCCESS;
        }

        /// <summary>
        ///     SSL_CTX_set_cookie_verify_cb (no docs)
        /// </summary>
        /// <param name="cb"></param>
        public void SetVerifyCookieCallback(VerifyCookieCallback cb)
        {
            _verifyCookieCallback = cb;
            _verifyCookieDelegate = VerifyCookieThunk;

            Native.SSL_CTX_set_cookie_verify_cb(Handle, _verifyCookieDelegate);
        }

        public int VerifyCookieThunk(IntPtr sslPtr, string cookie, int cookieLength)
        {
            var ssl = new Ssl(sslPtr);
            var c = new byte[cookieLength];

            //Marshal.Copy(cookie, c, 0, cookieLength);

            if (_verifyCookieCallback(ssl, Encoding.ASCII.GetBytes(cookie)))
                verifyCookieResult = Native.SUCCESS;
            else
                verifyCookieResult = Native.FAIL;

            return verifyCookieResult;
        }


        /// <summary>
        ///     SSL_CTX_set_options <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_options" />
        /// </summary>
        /// <param name="options">Options</param>
        public void SetOptions(SslOptions opts)
        {
            Native.SSL_CTX_set_options(Handle, (long) opts);
        }

        /// <summary>
        ///     SSL_CTX_clear_options <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_clear_options" />
        /// </summary>
        /// <param name="opts"></param>
        public void ClearOptions(SslOptions opts)
        {
            Native.SSL_CTX_clear_options(Handle, (long) opts);
        }

        /// <summary>
        ///     SSL_CTX_get_options <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_get_options" />
        /// </summary>
        /// <returns></returns>
        public SslOptions GetOptions()
        {
            return (SslOptions) Native.SSL_CTX_get_options(Handle);
        }

        /// <summary>
        ///     SSL_CTX_free <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_free" />
        /// </summary>
        public override void Dispose()
        {

        }

        ~Context()
        {
            Native.SSL_CTX_free(Handle);
        }

        #region Properties

        /// <summary>
        ///     SSL_CTX_get/set_max_protocol_version (macro)
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_get_max_proto_version" />
        /// </summary>
        public ProtocolVersion MaxProtocolVersion
        {
            get => (ProtocolVersion) Native.SSL_CTX_ctrl(Handle,
                (int) SslControlOperation.SSL_CTRL_GET_MAX_PROTO_VERSION, 0, IntPtr.Zero);
            set => Native.SSL_CTX_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_SET_MAX_PROTO_VERSION, (int) value,
                IntPtr.Zero);
        }

        /// <summary>
        ///     SSL_CTX_get/set_min_protocol_version
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_get_min_proto_version" />
        /// </summary>
        public ProtocolVersion MinProtocolVersion
        {
            get => (ProtocolVersion) Native.SSL_CTX_ctrl(Handle,
                (int) SslControlOperation.SSL_CTRL_GET_MIN_PROTO_VERSION, 0, IntPtr.Zero);
            set => Native.SSL_CTX_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_SET_MIN_PROTO_VERSION, (int) value,
                IntPtr.Zero);
        }

        /// <summary>
        ///     SSL_CTX_get/set_session_cache_mode (macro), uses SSL_CTX_ctrl
        ///     <see cref="https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_session_cache_mode" />
        /// </summary>
        public SslSessionCacheMode SessionCacheMode
        {
            get =>
                (SslSessionCacheMode) Native.SSL_CTX_ctrl(Handle,
                    (int) SslControlOperation.SSL_CTRL_GET_SESS_CACHE_MODE, 0, IntPtr.Zero);
            set => Native.SSL_CTX_ctrl(Handle, (int) SslControlOperation.SSL_CTRL_SET_SESS_CACHE_MODE, (long) value,
                IntPtr.Zero);
        }

        #endregion
    }
}