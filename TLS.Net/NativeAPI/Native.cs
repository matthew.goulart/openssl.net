﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.NativeAPI
{
    internal static class Native
    {
#if WIN32
        private const string SSLDLLNAME = "libssl-1_1.dll";
        private const string DLLNAME = "libcrypto-1_1.dll";
#elif WIN64
        private const string SSLDLLNAME = "Binaries/Win/x64/libssl-1_1-x64.dll";
        private const string DLLNAME = "Binaries/Win/x64/libcrypto-1_1-x64.dll";
#elif LINUX
        private const string SSLDLLNAME = "libssl.so.3";
        private const string DLLNAME = "libcrypto.so.3";
#endif

        public const int SUCCESS = 1;
        public const int FAIL = 0;

        //Library initialization
        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int OPENSSL_init_ssl();


        #region Context

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_CTX_clear_options(IntPtr ctx, long options);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_CTX_ctrl(IntPtr ctx, int cmd, long larg, IntPtr parg);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_CTX_free(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_get_max_proto_version(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_get_min_proto_version(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_CTX_get_options(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_load_verify_locations(IntPtr ctx, string file, string path);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSL_CTX_new(IntPtr sslMethod);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_cipher_list(IntPtr ctx, string cipherList);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_ciphersuites(IntPtr ctx, string cipherSuites);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_CTX_set_cookie_generate_cb(IntPtr ctx, GenerateCookieDelegate callback);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_CTX_set_cookie_verify_cb(IntPtr ctx, VerifyCookieDelegate callback);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_max_proto_version(IntPtr ctx, int version);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_min_proto_version(IntPtr ctx, int version);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_CTX_set_options(IntPtr ctx, long options);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_CTX_set_stateless_cookie_generate_cb(IntPtr ctx, IntPtr del);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_CTX_set_stateless_cookie_verify_cb(IntPtr ctx, IntPtr del);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_verify(IntPtr ctx, int mode, VerifyCertCallback callback);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_set_verify_depth(IntPtr ctx, int depth);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_use_PrivateKey_file(IntPtr ctx, string file, int type);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_use_RSAPrivateKey_file(IntPtr ctx, string file, int type);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_CTX_use_certificate_file(IntPtr ctx, string file, int type);

        #endregion

        #region delegates

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int gen_stateless_cookie_cb(IntPtr ssl, out byte[] cookie, ref int cookie_len);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int verify_stateless_cookie_cb(IntPtr ssl, byte[] cookie, int cookie_len);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int GenerateCookieDelegate(IntPtr ssl, IntPtr cookie, ref int cookie_len);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int VerifyCookieDelegate(IntPtr ssl, [MarshalAs(UnmanagedType.LPStr)] string cookie,
            int cookie_len);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int VerifyCertCallback(int ok, IntPtr x509_store_ctx);

        #endregion

        #region SSL

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_is_server(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_is_init_finished(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_peek(IntPtr ssl, byte[] buf, int num);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSL_new(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_ctrl(IntPtr ssl, int cmd, long larg, IntPtr parg);
        
        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_shutdown(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_set_bio(IntPtr ssl, IntPtr rbio, IntPtr wbio);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_set0_rbio(IntPtr ssl, IntPtr rbio);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_set0_wbio(IntPtr ssl, IntPtr rbio);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_free(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSL_get_rbio(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSL_get_wbio(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_set_fd(IntPtr ssl, IntPtr fd);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_get_fd(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern SslHandshakeState SSL_get_state(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_set_mode(IntPtr ssl, long opt);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long SSL_set_options(IntPtr ssl, long opt);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_connect(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_accept(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DTLSv1_listen(IntPtr ssl, IntPtr bioAddr);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_do_handshake(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_set_connect_state(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SSL_set_accept_state(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_read(IntPtr ssl, byte[] buf, int length);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_write(IntPtr ssl, byte[] buf, int length);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_set_cipher_list(IntPtr ssl, string cipherList);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_set_ciphersuites(IntPtr ssl, string cipherSuites);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern string SSL_get_cipher_name(IntPtr ssl);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SSL_get_error(IntPtr ssl, int ret);

        #endregion

        #region SSL Methods

        #region Version-agnostic methods

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLS_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLS_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLS_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLS_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLS_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLS_client_method();

        #endregion

        #region Deprecated methods

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv2_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv2_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv2_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv3_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv3_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SSLv3_client_method();

        //These methods don't actually exist anymore and point to the version agnostic methods above
        //[DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        //public extern static IntPtr SSLv23_method();

        //[DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        //public extern static IntPtr SSLv23_server_method();

        //[DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        //public extern static IntPtr SSLv23_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_1_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_1_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_1_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_2_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_2_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr TLSv1_2_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_server_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_2_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_2_client_method();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr DTLSv1_2_server_method();

        #endregion

        #endregion

        #region X509Context

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr X509_STORE_CTX_new();

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void X509_STORE_CTX_cleanup(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void X509_STORE_CTX_free(IntPtr ctx);

        [DllImport(SSLDLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int X509_STORE_CTX_init(IntPtr ctx);

        #endregion

        #region BIO

        #region BIO Creation

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_new(IntPtr type);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_s_mem();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_s_socket();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_s_datagram();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_method_type(IntPtr b);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        //!!public extern static IntPtr BIO_new_file(byte[] filename, byte[] mode);
        public static extern IntPtr BIO_new_file(string filename, string mode);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_new_mem_buf(byte[] buf, int len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_new_socket(IntPtr socket, int close_flag);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_new_dgram(IntPtr socket, int close_flag);

        #endregion

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_f_md();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_f_null();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_ADDR_new();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_set_fd(IntPtr bio, IntPtr fd, int close);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void BIO_set_close(IntPtr bio, int close);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_get_conn_address(IntPtr bio);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern long BIO_set_conn_address(IntPtr bio, IntPtr bioAddr);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr BIO_push(IntPtr bp, IntPtr append);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_ctrl(IntPtr bp, int cmd, int larg, IntPtr parg);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_int_ctrl(IntPtr bp, int cmd, int larg, int parg);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_read(IntPtr b, byte[] buf, int len);

        //Might no work...
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_read(IntPtr b, IntPtr buf, int len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_write(IntPtr b, byte[] buf, int len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_write(IntPtr b, IntPtr buf, int len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_puts(IntPtr b, byte[] buf);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int BIO_gets(IntPtr b, byte[] buf, int len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void BIO_free(IntPtr bio);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern uint BIO_number_read(IntPtr bio);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern uint BIO_number_written(IntPtr bio);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern uint BIO_ctrl_pending(IntPtr bio);

        #endregion

        #region Crypto
        
        #region EVP
        
        #region SHA-2

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha1();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha224();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha512_224();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha512_256();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha384();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_sha512();
        #endregion
        
        #region MD#
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_md2();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_md4();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_md5();
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EVP_md5_sha1();
        
        #endregion
        
        #endregion
        
        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int HMAC(IntPtr evp_md, byte[] key, int key_len, byte[] d, int n, byte[] md, ref int md_len);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int RAND_bytes(byte[] buf, int num);
        
        #endregion

        #region Error

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern ulong ERR_get_error();

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern StringBuilder ERR_error_string(ulong e, StringBuilder buf);

        [DllImport(DLLNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ERR_error_string_n(ulong e, byte[] buf, int len);

        #endregion
    }
}