using System;

namespace TLS.Net.NativeAPI.Crypto
{
    public class EVP : NativeObject
    {
        public const int EVP_MAX_MD_SIZE = 1024;

        private EVP(IntPtr handle) : base(handle)
        {
        }

        public static EVP SHA1 => new EVP(Native.EVP_sha1());

        public override void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}