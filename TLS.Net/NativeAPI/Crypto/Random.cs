using System;

namespace TLS.Net.NativeAPI.Crypto
{
    public static class Random
    {
        public static byte[]GetRandomBytes(int length)
        {
            var buf = new byte[length];

            Native.RAND_bytes(buf, length);

            return buf;
        }
    }
}