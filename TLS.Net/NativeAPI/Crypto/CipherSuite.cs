using System;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.NativeAPI.Crypto
{
    public class CipherSuite : NativeObject
    {
        private CipherSuite(IntPtr handle, string name, ProtocolVersion protocolVersion,
            KeyExchangeMethod keyExchangeMethod, AuthenticationMethod authenticationMethod,
            EncryptionMethod encryptionMethod, MACType macType)
            : base(handle)
        {
            Name = name;
            MinimumProtocolVersion = protocolVersion;
            KeyExchangeMethod = keyExchangeMethod;
            AuthenticationMethod = authenticationMethod;
            EncryptionMethod = encryptionMethod;
            MacType = macType;
        }

        public string Name { get; }
        public ProtocolVersion MinimumProtocolVersion { get; }
        public KeyExchangeMethod KeyExchangeMethod { get; }
        public AuthenticationMethod AuthenticationMethod { get; }
        public EncryptionMethod EncryptionMethod { get; }
        public MACType MacType { get; }

        //Suites


        public override void Dispose()
        {
            //Nothing to clean up here...
        }
    }
}