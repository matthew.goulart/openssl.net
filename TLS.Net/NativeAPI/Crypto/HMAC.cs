using System;
using System.Runtime.InteropServices;

namespace TLS.Net.NativeAPI.Crypto
{
    public static class HMAC
    {
        public static ArraySegment<byte> SHA1(byte[] key, byte[] msg)
        {
            return GetHMAC(EVP.SHA1, key, msg);
        }
        public static ArraySegment<byte> GetHMAC(EVP evp, byte[] key, byte[] msg)
        {
            byte[] buf = new byte[EVP.EVP_MAX_MD_SIZE];
            int length = 0;

            Native.HMAC(evp, key, key.Length, msg, msg.Length, buf, ref length);

            return new ArraySegment<byte>(buf, 0, length);
        }
    }
}