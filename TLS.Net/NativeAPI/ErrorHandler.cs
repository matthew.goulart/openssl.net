using System;
using System.Text;

namespace TLS.Net.NativeAPI
{
    public static class ErrorHandler
    {
        public static ulong GetError()
        {
            return Native.ERR_get_error();
        }

        public static string GetErrorString(ulong error)
        {
            var buf = new byte[256];
            byte term = 0;

            Native.ERR_error_string_n(error, buf, buf.Length);

            var termIndex = Array.IndexOf(buf, term);

            return Encoding.ASCII.GetString(buf, 0, termIndex);
        }
    }
}