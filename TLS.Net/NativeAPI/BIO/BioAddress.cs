using System;
using System.Net;
using TLS.Net.NativeAPI.Helpers;

namespace TLS.Net.NativeAPI.BIO
{
    public class BioAddress : NativeObject
    {
        public BioAddress(IntPtr handle)
            : base(handle)
        {
        }

        public BioAddress() : base(Native.BIO_ADDR_new())
        {
        }

        public static implicit operator IPEndPoint(BioAddress b)
        {
            return SockAddrHelper.SockAddrToIPEndPoint(b.Handle);
        }

        public static implicit operator BioAddress(IPEndPoint i)
        {
            return new BioAddress(SockAddrHelper.IPEndPointToSockAddr(i));
        }

        public IPEndPoint ToIPEndPoint()
        {
            return SockAddrHelper.SockAddrToIPEndPoint(Handle);
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}