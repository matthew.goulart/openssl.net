using System;
using System.Net.Sockets;

namespace TLS.Net.NativeAPI.BIO
{
    public class SocketBio : Bio
    {
        public SocketBio(IntPtr bio)
            : base(bio)
        {
        }

        public SocketBio(Socket socket, int close = BIO_CLOSE) : base(Native.BIO_new(Native.BIO_s_socket()))
        {
            Native.BIO_set_fd(Handle, socket.Handle, close);
            Native.BIO_set_close(Handle, 1);
        }

        public IntPtr GetConnAddr()
        {
            return Native.BIO_get_conn_address(Handle);
        }

        public long SetConnAddress(IntPtr bioAddr)
        {
            return Native.BIO_set_conn_address(Handle, bioAddr);
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}