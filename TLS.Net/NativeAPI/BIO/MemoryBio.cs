﻿using System;
using System.IO.Pipelines;

namespace TLS.Net.NativeAPI.BIO
{
    public class MemoryBio : Bio
    {
        private Pipe _pipe;

        public MemoryBio(IntPtr ptr)
            : base(ptr)
        {
        }

        public MemoryBio()
            : base(Native.BIO_new(Native.BIO_s_mem()))
        {
        }

        public override void Dispose()
        {
            //TODO: implement BIO_vfree();
        }
    }
}