﻿using System;

namespace TLS.Net.NativeAPI.BIO
{
    /// <summary>
    ///     Encapsulates the BIO_* functions.
    /// </summary>
    public class Bio : NativeObject
    {
        public const int BIO_NOCLOSE = 0;
        public const int BIO_CLOSE = 1;

        public Bio(IntPtr handle)
            : base(handle)
        {
        }

        public static Bio IntPtrToBio(IntPtr bio)
        {
            var type = (BioType) Native.BIO_method_type(bio);

            switch (type)
            {
                case BioType.BIO_TYPE_DGRAM:
                    return new DatagramBio(bio);
                case BioType.BIO_TYPE_FD:
                    return new SocketBio(bio);
                default:
                    return new Bio(bio);
            }
        }

        public uint BytesPending => Native.BIO_ctrl_pending(Handle);

        public int Read(byte[] buffer, int length)
        {
            return Native.BIO_read(Handle, buffer, length);
        }

        public int Read(IntPtr buffer, int length)
        {
            return Native.BIO_read(Handle, buffer, length);
        }

        public int Write(byte[] buffer, int length)
        {
            return Native.BIO_write(Handle, buffer, length);
        }

        public int Write(IntPtr buffer, int length)
        {
            return Native.BIO_write(Handle, buffer, length);
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        ~Bio()
        {
            Native.BIO_free(Handle);
        }
    }
}