using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using TLS.Net.NativeAPI.Helpers;

namespace TLS.Net.NativeAPI.BIO
{
    public class DatagramBio : Bio
    {
        public DatagramBio(IntPtr ptr)
            : base(ptr)
        {
        }

        public DatagramBio(Socket socket, bool close = true)
            : base(Native.BIO_new_dgram(socket.Handle, close ? 1 : 0))
        {
        }

        /// <summary>
        /// BIO_dgram_get/set_peer (no ossl docs)
        /// Gets or sets the peer used by the <see cref="DatagramBio"/>
        /// </summary>
        /// <exception cref="SslException"></exception>
        public IPEndPoint Peer
        {
            get
            {
                var bioAddr = new BioAddress();
                
                Native.BIO_ctrl(Handle, (int)BioControlOperation.BIO_CTRL_DGRAM_GET_PEER, 0, bioAddr.Handle);
                
                return bioAddr;
            }
            set
            {
                var bioAddr = SockAddrHelper.IPEndPointToSockAddr(value);
                
                if(Native.BIO_ctrl(Handle, (int)BioControlOperation.BIO_CTRL_DGRAM_SET_PEER, 0, bioAddr) != Native.SUCCESS)
                    throw new SslException(ErrorHandler.GetError());
            }
        }

        public int SetFd(IntPtr fd, bool close = false)
        {
            return Native.BIO_int_ctrl(Handle, 104, close ? 1 : 0, (int)fd);
        }

        public void SetConnected(IntPtr bioAddrPtr)
        {
            Native.BIO_ctrl(Handle, (int) BioControlOperation.BIO_CTRL_DGRAM_SET_CONNECTED, 0, bioAddrPtr);
        }

    public override void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}