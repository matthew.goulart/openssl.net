using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.ManagedAPI.Dtls
{
    public class DtlsClient : SecureTransport
    {
        private IPEndPoint _ep;

        public DtlsClient(IPEndPoint remoteEp, Context ctx)
            : base(new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp), ctx)
        {
            _socket.Connect(remoteEp);
        }

        internal DtlsClient(Socket socket, Context ctx)
            : base(socket, ctx)
        {
        }

        internal DtlsClient(Socket socket, Ssl ssl)
            : base(socket, ssl)
        {
        }

        public async Task<ReadOnlyMemory<byte>> ReceiveAsync()
        {
            Memory<byte> mem = new byte[TLS_MAX_RECORD_SIZE];
            MemoryMarshal.TryGetArray(mem, out ArraySegment<byte> seg);
            
            var bytesRead = _ssl.Read(seg.Array, Ssl.TLS_MAX_RECORD_SIZE);

            return new ReadOnlyMemory<byte>(seg.Array, 0, bytesRead);
        }

        public async Task SendAsync(Memory<byte> buf)
        {
            MemoryMarshal.TryGetArray(buf, out ArraySegment<byte> seg);

            _ssl.Write(seg.Array, buf.Length);
        }
    }
}