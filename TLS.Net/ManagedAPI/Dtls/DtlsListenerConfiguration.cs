namespace TLS.Net.ManagedAPI.Dtls
{
    public class DtlsListenerConfiguration : IDtlsListenerConfiguration
    {
        public string _CertFile { get; private set; }
        public string _KeyFile { get; private set; }

        public IDtlsListenerConfiguration CertificateFile(string file)
        {
            _CertFile = file;

            return this;
        }

        public IDtlsListenerConfiguration KeyFile(string file)
        {
            _KeyFile = file;

            return this;
        }
    }
}