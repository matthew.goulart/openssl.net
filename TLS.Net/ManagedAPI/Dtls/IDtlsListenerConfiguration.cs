namespace TLS.Net.ManagedAPI.Dtls
{
    public interface IDtlsListenerConfiguration
    {
        IDtlsListenerConfiguration CertificateFile(string file);
        IDtlsListenerConfiguration KeyFile(string file);
    }
}