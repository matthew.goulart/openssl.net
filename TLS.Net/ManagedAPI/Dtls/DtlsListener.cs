using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using TLS.Net.NativeAPI;
using TLS.Net.NativeAPI.BIO;
using TLS.Net.NativeAPI.SSL;
using Crypto = TLS.Net.NativeAPI.Crypto;

namespace TLS.Net.ManagedAPI.Dtls
{
    public class DtlsListener
    {
        
        //Keep a copy of a BIO_ADDR for DTLSv1_listen()
        private readonly BioAddress _bioAddr = new BioAddress();
        private readonly Context _ctx;
        private readonly CancellationTokenSource _listenTaskCt;
        private readonly IPEndPoint _localEp;
        private readonly int _threads;

        private Task _listenTask;
        private BufferBlock<DtlsClient> _pendingClients;

        private byte[] cookieSecret;

        public DtlsListener(IPEndPoint localEp, string certFile, string keyFile, int threads = 2)
        {
            //Set up SSL context
            _ctx = new Context(SslMethod.DTLS_Server);
            _ctx.UseCertificateFile(certFile, CertificateType.PEM);
            _ctx.UsePrivateKeyFile(keyFile, CertificateType.PEM);
            _ctx.SetVerify(SslVerificationKind.SSL_VERIFY_PEER | SslVerificationKind.SSL_VERIFY_CLIENT_ONCE,
                VerifyCert);
            _ctx.SetGenerateCookieCallback(GenerateCookie);
            _ctx.SetVerifyCookieCallback(VerifyCookie);
            _ctx.SetOptions(SslOptions.SSL_OP_COOKIE_EXCHANGE);

            //Local vars
            _localEp = localEp;
            _threads = threads;
            _listenTaskCt = new CancellationTokenSource();
            
            //Since there are no cookie funcs provided, we need to set up our own
            cookieSecret = Crypto.Random.GetRandomBytes(16);
        }

        public void Start(int backlog = 5)
        {
            var options = new DataflowBlockOptions {BoundedCapacity = backlog};

            _pendingClients = new BufferBlock<DtlsClient>(options);
            
            var tasks = new List<Task>();

            for (int i = 0; i < _threads; i++)
            {
                tasks.Add(Task.Run(() => DoListen(_listenTaskCt.Token)));
            }

            _listenTask = Task.WhenAll(tasks);
        }

        public Task Stop()
        {
            _listenTaskCt.Cancel();
            _pendingClients.Complete();

            return _listenTask;
        }

        private async Task DoListen(CancellationToken ct = default)
        {
            var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            sock.Bind(_localEp);

            while (true)
            {
                var dgramBio = new DatagramBio(sock, false);
                var ssl = new Ssl(_ctx, dgramBio);
                var addr = new BioAddress();

                while (!Ssl.DtlsListen(ssl, addr))
                {
                    ct.ThrowIfCancellationRequested();
                    Debug.WriteLine("Received ClientHello with invalid or missing cookie, or invalid dgram");
                }
                
                Debug.WriteLine("DTLS Listener => Received ClientHello with correct cookie, creating new client...");
                
                var clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                clientSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                clientSock.Bind(_localEp);
                clientSock.Connect(addr);

                dgramBio.SetFd(clientSock.Handle, true);
                dgramBio.SetConnected(addr.Handle);
                
                var client = new DtlsClient(clientSock, ssl);

                //TODO: Fix this once msft figures out the socket issues
                sock.Close();

                await _pendingClients.SendAsync(client, ct);

                return;
            }
        }

        public async Task<DtlsClient> AcceptClientAsync(bool doAuth = false)
        {
            var client = await _pendingClients.ReceiveAsync();

            if (doAuth) await client.AuthenticateAsServerAsync();

            return client;
        }

        private ArraySegment<byte> GenerateCookie(Ssl ssl)
        {
            IPEndPoint remoteEp = ((DatagramBio) ssl.ReadBio).Peer;

            var cookie = Crypto.HMAC.SHA1(cookieSecret, Encoding.ASCII.GetBytes(remoteEp.ToString()));
            
            return cookie;
        }

        private bool VerifyCookie(Ssl ssl, byte[] cookie)
        {
            IPEndPoint remoteEp = ((DatagramBio) ssl.ReadBio).Peer;
            var hmac = Crypto.HMAC.SHA1(cookieSecret, Encoding.ASCII.GetBytes(remoteEp.ToString()));

            if (cookie.SequenceEqual(hmac));
                return true;

            return false;
        }

        private bool VerifyCert(int ok, X509Store store)
        {
            return true;
        }
    }
}