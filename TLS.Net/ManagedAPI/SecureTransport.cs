using System;
using System.Buffers;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using TLS.Net.NativeAPI;
using TLS.Net.NativeAPI.BIO;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.ManagedAPI
{
    public abstract class SecureTransport
    {
        protected const int TLS_MAX_RECORD_SIZE = 4096;
        protected readonly Socket _socket;
        protected readonly Ssl _ssl;

        protected SecureTransport(Socket socket, Context ctx)
        {
            _socket = socket;
            _ssl = new Ssl(ctx, new MemoryBio(), new MemoryBio());
        }

        protected SecureTransport(Socket socket, Ssl ssl)
        {
            _socket = socket;
            _ssl = ssl;
        }

        public bool IsAuthenticated => _ssl.HandshakeState == SslHandshakeState.TLS_ST_OK;

        internal bool IsAcceptStateSet { get; set; } = false;

        /// <summary>
        ///     Attempts to negotiate an SSL/TLS session with a connected client
        /// </summary>
        /// <returns>
        ///     An awaitable <see cref="Task" /> that completes when the SSL/TLS session has been established or when
        ///     negotiation has failed.
        /// </returns>
        /// <exception cref="SslException"></exception>
        public virtual async Task AuthenticateAsServerAsync()
        {
            if (IsAuthenticated)
                throw new InvalidOperationException("Cannot Accept(). Transport is already authenticated");
            
            //This may have already been set by DTLSv1_listen
            //if(!IsAcceptStateSet)
                //_ssl.SetAcceptState();

            _ssl.Accept();

            //await ConnectOrAcceptAsync();
        }

        public virtual async Task AuthenticateAsClientAsync()
        {
            if (IsAuthenticated)
                throw new InvalidOperationException("Cannot Connect(). Transport is already authenticated");

            _ssl.SetConnectState();

            await ConnectOrAcceptAsync();
        }

        protected virtual async Task ConnectOrAcceptAsync()
        {
            //Set up the buffer to work with the various APIs
            var owner = MemoryPool<byte>.Shared.Rent(TLS_MAX_RECORD_SIZE);
            var mem = owner.Memory;

            MemoryMarshal.TryGetArray(mem, out ArraySegment<byte> seg);
            var arr = seg.Array;

            while (!_ssl.IsInitFinished)
            {
                Debug.WriteLine($"Current handshake state is {_ssl.HandshakeState}");
                
                var acc = _ssl.DoHandshake();
                var result = _ssl.GetError(acc);
                if (result == SslError.SSL_ERROR_SYSCALL) throw new SslException(ErrorHandler.GetError());

                Debug.WriteLine($"Auth result was: {result}");

                if (_ssl.WriteBio.BytesPending > 0)
                {
                    Debug.WriteLine($"{_ssl.WriteBio.BytesPending} bytes pending in write bio");
                    var bytesRead = _ssl.WriteBio.Read(arr, TLS_MAX_RECORD_SIZE);
                    Debug.WriteLine($"{bytesRead} bytes read from write bio");
                    var bytesSent = await _socket.SendAsync(mem.Slice(0, bytesRead), SocketFlags.None);
                    Debug.WriteLine($"{bytesSent} bytes sent to client");

                    if (_ssl.IsInitFinished) break;
                }
                
                var bytesRcvd = await _socket.ReceiveAsync(mem, SocketFlags.None);
                Debug.WriteLine($"{bytesRcvd} bytes read from socket.");
                var bytesWritten = _ssl.ReadBio.Write(arr, bytesRcvd);
                Debug.WriteLine($"{bytesWritten} bytes written to SSL");
                Debug.Assert(bytesRcvd == bytesWritten);
            }

            owner.Dispose();
        }

        public virtual async Task ShutdownAsync()
        {
            _ssl.Shutdown();
        }
    }
}