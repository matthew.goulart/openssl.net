using System.Net;
using System.Net.Sockets;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;
using TLS.Net.NativeAPI;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.ManagedAPI.Tls
{
    public class TlsListener
    {
        private readonly Context _ctx;
        private readonly Socket _sock;

        public TlsListener(IPEndPoint localEp, string certFile, string keyFile)
        {
            _ctx = new Context(SslMethod.TLS_Server);
            _ctx.UseCertificateFile(certFile, CertificateType.PEM);
            _ctx.UsePrivateKeyFile(keyFile, CertificateType.PEM);

            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _sock.Bind(localEp);
            _sock.Listen(25);
        }

        public TlsListener(IPEndPoint localEp, string certFile, string keyFile, ProtocolVersion minProtoV,
            ProtocolVersion maxProtoV)
            : this(localEp, certFile, keyFile)
        {
            _ctx.MinProtocolVersion = minProtoV;
            _ctx.MaxProtocolVersion = maxProtoV;
        }

        public async Task<TlsClient> AcceptClientAsync(bool doAuth = false)
        {
            var clientSock = await _sock.AcceptAsync();

            var client = new TlsClient(clientSock, _ctx);

            if (doAuth) await client.AuthenticateAsServerAsync();

            return client;
        }
    }
}