using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using TLS.Net.Util;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.ManagedAPI.Tls
{
    public class TlsClient : SecureTransport, IDuplexPipe
    {
        private readonly Pipe _inputPipe, _outputPipe;
        private Task _runTask;

        /// <summary>
        /// Creates a new <see cref="TlsClient"/> and connects the underlying socket to <see cref="remoteEp"/>
        /// </summary>
        /// <param name="remoteEp">The remote endpoint to connect the underlying socket to</param>
        /// <param name="ctx">The <see cref="Context"/> to use for creation of an <see cref="Ssl"/> object</param>
        public TlsClient(IPEndPoint remoteEp, Context ctx)
            : this(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp), ctx)
        {
            _socket.Connect(remoteEp);
        }

        internal TlsClient(Socket socket, Context ctx)
            : base(socket, ctx)
        {
            _inputPipe = new Pipe();
            _outputPipe = new Pipe();
        }

        internal TlsClient(Socket socket, Ssl ssl)
            : base(socket, ssl)
        {
            _inputPipe = new Pipe();
            _outputPipe = new Pipe();
        }

        public PipeReader Input => _inputPipe.Reader;
        public PipeWriter Output => _outputPipe.Writer;

        /// <summary>
        /// Performs the appropriate handshake for the given protocol.
        /// Once the handshake is completed successfully, will kick off the
        /// receive and send loops
        /// </summary>
        /// <returns>A task that will be completed once the handshake is finished</returns>
        protected override async Task ConnectOrAcceptAsync()
        {
            await base.ConnectOrAcceptAsync();

            var rxTask = Task.Run(DoSendAsync);
            var txTask = Task.Run(DoReceiveAsync);

            _runTask = Task.WhenAll(rxTask, txTask);
        }

        /// <summary>
        /// Asynchronous send loop. Will write data from the <see cref="Ssl"/> to the <see cref="Socket"/>
        /// until the pipe is completed or a fatal error occurs
        /// </summary>
        /// <returns>A task that can be completed by completing the output <see cref="Pipe"/>'s <see cref="PipeReader"/></returns>
        private async Task DoSendAsync()
        {
            var reader = _outputPipe.Reader;

            //Make a new buffer for the outgoing data
            var outMemOwner = MemoryPool<byte>.Shared.Rent(TLS_MAX_RECORD_SIZE);
            var outMem = outMemOwner.Memory;
            MemoryMarshal.TryGetArray(outMem, out ArraySegment<byte> outBuf);

            Exception ex = null;

            try
            {
                while (true)
                {
                    var read = await reader.ReadAsync();

                    if (read.IsCanceled || read.IsCompleted && read.Buffer.IsEmpty)
                        break;

                    //Read from the pipe and make a buffer we can work with
                    var seq = read.Buffer;
                    var mem = seq.GetContiguousMemory();
                    MemoryMarshal.TryGetArray(mem, out var buf);

                    //Push the data through OpenSSL
                    _ssl.Write(buf.Array, (int) seq.Length);
                    var bytesRead = _ssl.WriteBio.Read(outBuf.Array, TLS_MAX_RECORD_SIZE);

                    //Write to the socket
                    await _socket.SendAsync(outMem.Slice(0, bytesRead), SocketFlags.None);

                    //Advance the reader
                    reader.AdvanceTo(seq.End);
                }
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                reader.Complete(ex);
                outMemOwner.Dispose();
            }
        }

        /// <summary>
        /// Asynchronous receive loop. Will read from the socket and push data
        /// through OpenSSL until the pipe is completed or a fatal error occurs
        /// </summary>
        /// <returns>A task that can be completed by completing the input <see cref="Pipe"/>'s <see cref="PipeWriter"/></returns>
        private async Task DoReceiveAsync()
        {
            var writer = _inputPipe.Writer;
            Exception ex = null;

            try
            {
                while (true)
                {
                    var mem = writer.GetMemory(TLS_MAX_RECORD_SIZE);
                    MemoryMarshal.TryGetArray(mem, out ArraySegment<byte> buf);

                    //Since OpenSSL will only gve us a complete record, we need a func that will piece one back together from the stream
                    var read = await ReadRecord(buf);
                    
                    writer.Advance(read);

                    var flush = await writer.FlushAsync();

                    if (flush.IsCanceled || flush.IsCompleted)
                        break;
                }
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                writer.Complete(ex);
            }
        }

        /// <summary>
        /// Attempts to read from an <see cref="Ssl"/> until a whole record is available.
        /// Some hashing functions require a whole message present in order to decrypt.
        /// </summary>
        /// <param name="mem">The buffer where the record will be written</param>
        /// <returns>A task that will complete once a record is read</returns>
        private async Task<int> ReadRecord(ArraySegment<byte> buf)
        {
            SslError res;
            int sslRead = 0;
            
                do
                {
                    var sockRead = await _socket.ReceiveAsync(buf, SocketFlags.None);

                    if (sockRead == 0) break;

                    _ssl.ReadBio.Write(buf.Array, sockRead);
                    sslRead = _ssl.Read(buf.Array, TLS_MAX_RECORD_SIZE);

                    res = _ssl.GetError(sslRead);
                } while (res != SslError.SSL_ERROR_NONE);

                return sslRead;
        }

        /// <summary>
        /// Completes both <see cref="Pipe"/>s, alerts the client to a shutdown and closes the underlying <see cref="Socket"/>
        /// </summary>
        /// <returns>A task that will complete when all components have successfully shutdown/stopped</returns>
        public override Task ShutdownAsync()
        {
            Input.Complete();
            Output.Complete();
            
            var res = base.ShutdownAsync();
            
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();

            return _runTask;
        }
        
    }
}