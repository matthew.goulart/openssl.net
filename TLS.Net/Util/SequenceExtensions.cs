using System;
using System.Buffers;

namespace TLS.Net.Util
{
    public static class SequenceExtensions
    {
        public static ReadOnlyMemory<T> GetContiguousMemory<T>(this ReadOnlySequence<T> buffer)
        {
            //TODO: This function is evil unless it is EXCEEDINGLY rare that the buffer is multiple segments.
            // ToArray() is, relatively speaking, horribly slow.
            if (buffer.IsSingleSegment)
                return buffer.First;
            return buffer.ToArray(); //This is sacrilegious and should be avoided
        }
    }
}