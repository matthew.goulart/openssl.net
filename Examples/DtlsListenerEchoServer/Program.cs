﻿using System;
using System.Buffers;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using TLS.Net.ManagedAPI.Dtls;

namespace DtlsListenerEchoServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Start();

            task.Wait();
        }

        public static async Task Start()
        {
            var listener = new DtlsListener(new IPEndPoint(IPAddress.Loopback, 1114), 
                "certs/cert.crt",
                "certs/key.key");

            listener.Start();

            var client = await listener.AcceptClientAsync();

            await client.AuthenticateAsServerAsync();

            while (true)
            {
                var msg = await client.ReceiveAsync();
                await client.SendAsync(MemoryMarshal.AsMemory(msg));
            }
        }
    }
}
