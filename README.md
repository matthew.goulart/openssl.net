TLS.Net is 2 things:
1. A wrapper around the TLS/DTLS functionality of OpenSSL
2. An easy-to-use TLS/DTLS library with a familiar API

Features:
- TLS v 1.0 to 1.3
- DTLS v 1.0 and 1.2
- All cyphers supported by OpenSSL v1.1.1c