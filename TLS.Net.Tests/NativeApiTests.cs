using System;
using System.Buffers;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLS.Net.NativeAPI;
using TLS.Net.NativeAPI.BIO;
using TLS.Net.NativeAPI.SSL;

namespace TLS.Net.Tests
{
    [TestClass]
    public class NativeApiTests
    {
        [TestMethod]
        public void DtlsTest()
        {
            var localEp = new IPEndPoint(IPAddress.Loopback, 1114);
            
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            setsockopt(socket.Handle, 1, 2, 0, sizeof(int));
            socket.Bind(localEp);
            
            var ctx = new Context(SslMethod.DTLS);
            ctx.UseCertificateFile("certs/cert.crt", CertificateType.PEM);
            ctx.UsePrivateKeyFile("certs/key.key", CertificateType.PEM);
            ctx.SetVerify(SslVerificationKind.SSL_VERIFY_PEER | SslVerificationKind.SSL_VERIFY_CLIENT_ONCE,
                VerifyCert);
            ctx.SetGenerateCookieCallback(GenerateCookie);
            ctx.SetVerifyCookieCallback(VerifyCookie);
            ctx.SetOptions(SslOptions.SSL_OP_COOKIE_EXCHANGE);
            
            var dgramBio = new DatagramBio(socket, false);
            var ssl = new Ssl(ctx, dgramBio);
            
            var bioAddr = new BioAddress();

            while (!Ssl.DtlsListen(ssl, bioAddr))
            {
                Debug.WriteLine("Received ClientHello with invalid or missing cookie");
            }
            
            var clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            setsockopt(clientSocket.Handle, 1, 2, 0, sizeof(int));
            clientSocket.Bind(localEp);
            clientSocket.Connect(bioAddr);

            dgramBio.SetFd(clientSocket.Handle);
            dgramBio.SetConnected(bioAddr);

            ssl.Accept();

            ssl.Shutdown();
        }
        
        
        [DllImport("libc", SetLastError = true)]
        private static extern int setsockopt(IntPtr socket, int level, int option_name, int option_value, uint option_len);
        
        [TestMethod]
        public async Task BasicDtlsTest()
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            socket.Bind(new IPEndPoint(IPAddress.Loopback, 1114));

            var ctx = new Context(SslMethod.DTLS);
            ctx.UseCertificateFile("certs/cert.crt", CertificateType.PEM);
            ctx.UsePrivateKeyFile("certs/key.key", CertificateType.PEM);
            ctx.SetVerify(SslVerificationKind.SSL_VERIFY_PEER | SslVerificationKind.SSL_VERIFY_CLIENT_ONCE,
                VerifyCert);
            ctx.SetGenerateCookieCallback(GenerateCookie);
            ctx.SetVerifyCookieCallback(VerifyCookie);
            ctx.SetOptions(SslOptions.SSL_OP_COOKIE_EXCHANGE);

            var ssl = new Ssl(ctx, new MemoryBio(), new MemoryBio());

            var remoteEp = new IPEndPoint(IPAddress.Any, 0);

            var owner = MemoryPool<byte>.Shared.Rent(4096);
            var mem = owner.Memory;
            MemoryMarshal.TryGetArray(mem, out ArraySegment<byte> seg);
            var buf = seg.Array;

            SocketReceiveFromResult rcv;
            BioAddress bioAddr = new BioAddress();

            rcv = await socket.ReceiveFromAsync(seg, SocketFlags.None, new IPEndPoint(IPAddress.Any, 0));
            ssl.ReadBio.Write(buf, rcv.ReceivedBytes);

            Ssl.DtlsListen(ssl, bioAddr);

            var bytesRead = ssl.WriteBio.Read(buf, Ssl.TLS_MAX_RECORD_SIZE);
            await socket.SendToAsync(seg.Slice(0, bytesRead), SocketFlags.None, rcv.RemoteEndPoint);

            rcv = await socket.ReceiveFromAsync(seg, SocketFlags.None, new IPEndPoint(IPAddress.Any, 0));
            ssl.ReadBio.Write(buf, rcv.ReceivedBytes);

            Assert.IsTrue(Ssl.DtlsListen(ssl, bioAddr));

            socket.Close();

            var clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            clientSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            clientSock.Bind(new IPEndPoint(IPAddress.Loopback, 1114));
            clientSock.Connect(rcv.RemoteEndPoint);

            while (!ssl.IsInitFinished)
            {
                Debug.WriteLine($"Current handshake state is {ssl.HandshakeState}");

                int bytesRcvd = await clientSock.ReceiveAsync(mem, SocketFlags.None);
                Debug.WriteLine($"{bytesRcvd} bytes read from socket.");
                int bytesWritten = ssl.ReadBio.Write(buf, bytesRcvd);
                Debug.WriteLine($"{bytesWritten} bytes written to SSL");
                Debug.Assert(bytesRcvd == bytesWritten);

                int acc = ssl.Accept();
                SslError result = ssl.GetError(acc);
                if (result == SslError.SSL_ERROR_SYSCALL) throw new SslException(ErrorHandler.GetError());

                Debug.WriteLine($"Auth result was: {result}");

                if (ssl.WriteBio.BytesPending > 0)
                {
                    Debug.WriteLine($"{ssl.WriteBio.BytesPending} bytes pending in write bio");
                    bytesRead = ssl.WriteBio.Read(buf, 4096);
                    Debug.WriteLine($"{bytesRead} bytes read from write bio");
                    int bytesSent = await clientSock.SendAsync(mem.Slice(0, bytesRead), SocketFlags.None);
                    Debug.WriteLine($"{bytesSent} bytes sent to client");
                }
            }

            ssl.Shutdown();

            if (ssl.WriteBio.BytesPending > 0)
            {
                Debug.WriteLine($"{ssl.WriteBio.BytesPending} bytes pending in write bio");
                bytesRead = ssl.WriteBio.Read(buf, 4096);
                Debug.WriteLine($"{bytesRead} bytes read from write bio");
                int bytesSent = await clientSock.SendAsync(mem.Slice(0, bytesRead), SocketFlags.None);
                Debug.WriteLine($"{bytesSent} bytes sent to client");
            }
        }

        private ArraySegment<byte> GenerateCookie(Ssl ssl)
        {
            return Encoding.ASCII.GetBytes("testCookie");
        }

        private bool VerifyCookie(Ssl ssl, byte[] cookie)
        {
            if (cookie.SequenceEqual(Encoding.ASCII.GetBytes("testCookie")))
                return true;

            return false;
        }

        private bool VerifyCert(int ok, X509Store store)
        {
            return true;
        }
    }
}