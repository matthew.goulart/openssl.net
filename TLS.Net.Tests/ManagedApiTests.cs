using System;
using System.Buffers;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLS.Net.ManagedAPI.Dtls;
using TLS.Net.ManagedAPI.Tls;
using TLS.Net.NativeAPI;
using TLS.Net.NativeAPI.BIO;
using TLS.Net.NativeAPI.Helpers;
using TLS.Net.NativeAPI.SSL;
using TLS.Net.Util;

namespace TLS.Net.Tests
{
    [TestClass]
    public class ManagedApiTests
    {
        [TestMethod]
        public async Task BasicTlsListenerTest()
        {
            var listener = new TlsListener(new IPEndPoint(IPAddress.Loopback, 1114),
                "certs/cert.crt",
                "certs/key.key");

            Debug.WriteLine("Server => Signalling ready to accept client");

            var client = await listener.AcceptClientAsync();

            Debug.WriteLine($"Server => Client session started");

            await client.AuthenticateAsServerAsync();

            Debug.WriteLine("Server => Client successfully authenticated");

            var read = await client.Input.ReadAsync();

            var rxMem = read.Buffer.GetContiguousMemory();

            Assert.AreEqual("test", Encoding.Default.GetString(rxMem.Span.Slice(0, 4)));

            await client.Output.WriteAsync(rxMem);
            await client.Output.FlushAsync();

            await client.ShutdownAsync();
        }

        [TestMethod]
        public async Task BasicDtlsListenerTest()
        {
            var listener = new DtlsListener(new IPEndPoint(IPAddress.Loopback, 1114),
                "certs/cert.crt",
                "certs/key.key",
                1);
            listener.Start();

            Debug.WriteLine("Server => Signalling ready to accept client");

            var client = await listener.AcceptClientAsync();

            Debug.WriteLine($"Server => Client session started");

            await client.AuthenticateAsServerAsync();

            Debug.WriteLine("Server => Client successfully authenticated");

            var msg = await client.ReceiveAsync();

            Assert.IsTrue(msg.Length >= 4 && msg.Length <= 5);
            Assert.AreEqual("test", Encoding.Default.GetString(msg.Span.Slice(0, 4)));

            await client.SendAsync(MemoryMarshal.AsMemory(msg));

            await client.ShutdownAsync();

            await listener.Stop();
        }

        private byte[] GenerateCookie(Ssl ssl)
        {
            return Encoding.ASCII.GetBytes("testCookie");
        }

        private bool VerifyCookie(Ssl ssl, byte[] cookie)
        {
            if (cookie.SequenceEqual(Encoding.ASCII.GetBytes("testCookie")))
                return true;

            return false;
        }

        private bool VerifyCert(int ok, X509Store store)
        {
            return true;
        }
    }
}